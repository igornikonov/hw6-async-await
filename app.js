const ipBUtton = document.querySelector('.btn');
ipBUtton.addEventListener('click', getInfo);

const ipUrl = 'https://api.ipify.org/?format=json';
const infoUrl = 'http://ip-api.com/json/';
const neededFields = '?fields=status,message,continent,country,region,regionName,city,district';

async function getInfo() {
  const { ip } = await fetch(ipUrl).then(r => r.json());

  const data = await fetch(`${infoUrl}${ip}${neededFields}`).then(r => r.json());

  const { continent, country, region, regionName, city, district } = data;

  document.querySelector('.container').insertAdjacentHTML('beforeend', `
    <p>${continent}</p>
    <p>${country}</p>
    <p>${region}</p>
    <p>${regionName}</p>
    <p>${city}</p>
    <p>${district}</p>
  `);
}