class TrackByIP {
  constructor() {
    this.ipUrl = 'https://api.ipify.org/?format=json';
    this.ip = this.getIP();
    this.infoUrl = `http://ip-api.com/json/${this.ip}?fields=status,message,continent,country,region,regionName,city,district`
    this.ipButton = document.querySelector('.btn');
  }

  async getIP() {
    return await fetch(this.ipUrl).then(r => r.json());
  }

  async getInfo() {
    await this.getIP();
    const data = await fetch(this.infoUrl).then(r => r.json());
    console.log(data)
  }


}

new TrackByIP().getInfo();